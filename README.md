# XWSTestFiles

This is a collection of xws files for testing parser implementations.

For details of xws files, see here: https://github.com/elistevens/xws-spec

More details to come as I make them up...
